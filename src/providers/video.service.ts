import { UtilService } from './util.service';
import { Injectable } from "@angular/core";
import { Headers, Http, Response, } from "@angular/http";
import "rxjs/add/operator/toPromise";



@Injectable()
export class VideoService {

    constructor(public http: Http, public UtilService: UtilService,) {}

    ListarPorTag(tags: string): Promise<Response> {
        
        let host = this.UtilService.ObterHostDaApi();

        let headers = new Headers();
        headers.append('Content-type', 'application/json');

        return this.http.get(host + 'api/v1/Video/Listar/' + tags, { headers : headers}).toPromise();
    }

    ListarPorPlayList(idPlayList: string): Promise<Response> {
        
        let host = this.UtilService.ObterHostDaApi();
        
        let headers = new Headers();
        headers.append('Content-Type','application/json');

        return this.http.get(host + 'api/v1/video/Listar/' + idPlayList, { headers: headers}).toPromise();
    }

    Adicionar(request: any): Promise<Response> {

        let host = this.UtilService.ObterHostDaApi();
        let token = "Bearer " + localStorage.getItem('YouLearnToken');

        let headers = new Headers({ 
            "Content-Type": "application/json",
            "Authorization": token
        });
       

        return this.http.post(host + 'api/v1/Video/Adicionar', request, {headers: headers}).toPromise();
    }

}
