import { LoadingController, ToastController, AlertController, } from 'ionic-angular';
import { Injectable } from "@angular/core";


@Injectable()
export class UtilService {
    constructor(
        public loadCtrl: LoadingController,
        public toastCtrl: ToastController,
        public alertctrl: AlertController,     
        ) {
        }
        public showLoading(message: string = "Processando..."): any {
            let Loading = this.loadCtrl.create({
                content: message
            });
            return Loading;
        }
        
        // ENDEREÇO DA API 
        public ObterHostDaApi(): string {
            return "https://localhost:44340/";
        }
        
        public showToast(message:string, position: string = 'top'): any {
            let toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: position
            });
            toast.present();
        }
        
        public showAlert(message: string, title: string = "Operação realizada com sucesso."){
            let alert = this.alertctrl.create({
                title: title,
                subTitle: message,
                buttons: ['OK']
            });
            alert.present();
        }

        public showAlertCallback(message: string, title: string = "atenção"){
            let alert = this.alertctrl.create({
                title: title,
                subTitle: message,
                buttons: [{
                    text: 'OK'
                }]
            });
            alert.present();
        }

        public efetuarLogoff(){
            localStorage.clear();
        }
        
        showMessageError(response: Response){
            
            if (response.status == 0){
                this.showAlertCallback("Serviço Indisponivel!");
            }
            else if (response.status == 401){
                this.showAlertCallback("Autorização expirada, é necessário que se autentique novamente");
                
                localStorage.clear();
            }
            else if (response.status == 415){
                this.showToast("Desculpe, serviço Indisponivel, tente novamente mais tarde!");
            }
            else{
                
                let json : any = response.json();
                
                if(typeof json.error_description != 'undefined'){
                    this.showToast(json.error_description);
                }
                else if (typeof json.errors != 'undefined'){
                    
                    for (let erro in json.errors){
                        
                        let message = json.errors[erro].message;
                        
                        this.showToast(message);
                    }
                }
                else {
                    this.showToast("Operação falhou!")
                }
            }
            
        }
    } 