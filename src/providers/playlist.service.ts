import { UtilService } from './util.service';
import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import "rxjs/add/operator/toPromise";


@Injectable()
export class PlaylistService {
    
    constructor(public http: Http,
        public UtilService: UtilService,) { }
        
        listar(): Promise<Response> {
            let host = this.UtilService.ObterHostDaApi();
            let token = "Bearer "+ localStorage.getItem('YouLearnToken');
            let headers: any = new Headers();
            headers.append('Content-Type','application/json');
            headers.append('Authorization', token);
            
            return this.http.get(host + 'api/v1/PlayList/Listar',{ headers: headers}).toPromise();
        }
        
        adicionar(nome: string): Promise <Response> {
            let host = this.UtilService.ObterHostDaApi();
            let token = "Bearer "+ localStorage.getItem('YouLearnToken');
            let headers : any = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', token);

            return this.http.post(host + 'api/v1/PlayList/Adicionar/', {nome: nome}, {headers: headers}).toPromise();
        }

        excluir(id : any): Promise<Response> {
            let host = this.UtilService.ObterHostDaApi();
            let token = "Bearer "+ localStorage.getItem('YouLearnToken');
            let headers : any = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', token);

            return this.http.delete(host + 'api/v1/PlayList/Excluir/' + id, {headers: headers}).toPromise();
        }
    }
    
    
    