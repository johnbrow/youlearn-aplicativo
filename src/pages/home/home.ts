import { UsuarioService } from './../../providers/usuario.service';
import { VideoService } from './../../providers/video.service';
import { UtilService } from '../../providers/util.service';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  videos : any[] = [];
  constructor(public navCtrl: NavController, 
    private UtilService : UtilService,
    private VideoService: VideoService,
    private AlertController : AlertController,
    private UsuarioService : UsuarioService
    ) {
      
    }
    
    buscarVideo(tag: string) {
      console.log(tag);
      if (tag == null || tag.trim() == ""){
        return;
      }
      //Chama api e para obter os videos
      this.loadVideos(tag);
    }
    
    loadVideos(tag : string){
      // abrindo a tela de aguarde
      
      let loading = this.UtilService.showLoading();
      loading.present();
      
      this.VideoService.ListarPorTag(tag).then(
        (response)=>{
          
          // populo a lista de videos com um array
          this.videos = response.json();
          
          console.log(this.videos);
          
          // Fecho a tela de aguarde
          loading.dismiss();                         
        }
        ).catch((response)=>{
          this.UtilService.showMessageError(response);
        }
        
        );
      }
      
      showNovoVideo(){
        let token = localStorage.getItem('YouLearnToken')
        
        console.log('YouLearnToken=', token);
        
        if (token!= null) {
          // Se estiver autenticado , libera para tela de cadastro de videos
          this.navCtrl.push('VideoPage');
           console.log('YouLearnToken=', token);
        }
        else {
          let prompt = this.AlertController.create({
            title : 'Autenticar',
            message :  'Informe seus dados para se autenticar no sistema.',
            inputs: [
              {
                name : 'email',
                placeholder: 'E-mail',
                type: 'email',
                value: localStorage.getItem('usuario.email')
              },
              {
                name : 'senha',
                placeholder: 'Senha',
                type: 'password'
              }
            ],
            buttons:[
              {
                text: 'Novo usuario',
                handler: ()=>{
     
                  this.navCtrl.push('NovoUsuarioPage');
                }
              },
              {
                text: 'Entrar',
                handler: (data)=>{
                  console.log(data);
                  // AUTENTICAR USUARIO
                  
                  this.autenticarUsuario(data); 
                }
              }
            ]
            
          });
          prompt.present();
        }
      }
      
      autenticarUsuario(request : any){
        
        // ABRE A TELA DE AGUARDE
        let loading = this.UtilService.showLoading('Autenticando...');
        loading.present();
        
        this.UsuarioService.autenticar(request)
        .then((response)=>{
          let autenticado : boolean = response.json().authenticated;
          
          if(autenticado == false){
            this.UtilService.showToast("E-mail ou senha inválidos")
            loading.dismiss();
            return;
          }
          
          let token: string = response.json().accessToken;
          let primeiroNome: string = response.json().primeiroNome;

          // GUARDA O TOKEN NO LOCALSTORAGE
          localStorage.setItem('YouLearnToken',token);
          
          // FECHA A TELA
          loading.dismiss(); 
          
          // CRIANDO UMA TELA DE CONFIRMAÇÃO
          let confirm = this.AlertController.create({
            title : 'Oi ' + primeiroNome,
            message :  'Deseja criar um novo video?',
            buttons : [
              // SE NAO FECHA A TELA
              {
                text : 'Não', 
                handler: ()=>{}
              },
              // SE SIM ABRE A TELA DE VIDEO
              {
                text : 'Sim, eu quero ',
                handler: ()=>{
                  this.navCtrl.push('VideoPage');
                }
              }
            ]
            
          });
          confirm.present();
          
        })
        // AQUI SE DER ALGUM UM ERRO 
        .catch((response)=>{
          loading.dismiss(); 
          this.UtilService.showMessageError(response);          
        });
        
        
      }

      compartilharFacebook(video){
        window.open('https://www.facebook.com/sharer.php?u='+video.url);

      }

      showPlayList(video : any){
        this.navCtrl.push('PlayListPage', {idPlayList: video.idPlayList, nomePlayList: video.nomePlayList});
      }

      
    }
    