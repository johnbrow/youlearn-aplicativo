import { PlaylistService } from './../../providers/playlist.service';
import { VideoService } from './../../providers/video.service';
import { CanalService } from './../../providers/canal.service';
import { UtilService } from './../../providers/util.service';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage, } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {
  
  form : FormGroup;
  canais: any[] = [];
  playlists: any[] = [];
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private UtilService: UtilService,
    private modalCtrl : ModalController,
    private CanalService: CanalService,
    private VideoService: VideoService,
    private PlaylistService: PlaylistService,) 
    {
      
      this.form = this.fb.group({
        titulo: ['', Validators.compose([
          Validators.minLength(3),
          Validators.maxLength(100),
          Validators.required
        ])],
        
        descricao: ['', Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(300),
          Validators.required
        ])],
        
        idVideoYoutube: ['', Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(30),
          Validators.required
        ])],
        
        tags: ['', Validators.compose([
          Validators.minLength(1),
          Validators.maxLength(150),
          Validators.required
        ])],
        
        idCanal: ['', Validators.compose([
          Validators.required
        ])],
        
        idPlaylist: ['', Validators.compose([
          
        ])],
        
        ordemNaPlayList: ['', Validators.compose([
          
        ])],
        
      });
      
    }
    
    ionViewDidLoad() {
      this.loadCanal();
      this.loadPlayList();
    }
    
    loadCanal(){
      // ABRE A TELA DE AGUARDE
      let loading = this.UtilService.showLoading();
      loading.present();
      
      this.CanalService.listar()
      .then((response) => {
        this.canais = response.json();
        
        //FECHA A TELA AGUARDE
        loading.dismiss();
      })
      .catch((response) =>{
        
        //FECHA A TELA AGUARDE
        loading.dismiss();
        
        this.UtilService.showMessageError(response);
      });
    }
    
    loadPlayList(){
       // ABRE A TELA DE AGUARDE
       let loading = this.UtilService.showLoading();
       loading.present();
       
       this.PlaylistService.listar()
       .then((response) => {
         this.playlists = response.json();
         
         //FECHA A TELA AGUARDE
         loading.dismiss();
       })
       .catch((response) =>{
         
         //FECHA A TELA AGUARDE
         loading.dismiss();
         
         this.UtilService.showMessageError(response);
       });
    }

    salvar(){
      console.log(this.form.value);
      
      // ABRE A TELA DE AGUARDE
      let loading = this.UtilService.showLoading();
      loading.present();
      
      this.VideoService.Adicionar(this.form.value).then((response)=>{
        //FECHA A TELA DE AGUARDE
        loading.dismiss();
        console.log(response);
        
        this.UtilService.showAlert("Salvado com sucesso!");
        this.navCtrl.pop();
      }).catch((response)=>{
        //FECHA A TELA DE AGUARDE
        loading.dismiss();

        this.UtilService.showMessageError(response);
      });
    }
    
    cancelar(){
      this.navCtrl.pop();
    }
    
    showAddCanal(){
      let modal = this.modalCtrl.create('AdicionarCanalPage');
      
      modal.onDidDismiss(data=>{
        this.loadCanal();
      })
      modal.present();
    }
    
    // O ADICIONAR DO PLAYLIST QUE O PROFESSOR PULOU UMA AULA FDP
    showAddPlayList() {
      let modal = this.modalCtrl.create('AdicionarPlayListPage');
      
      modal.onDidDismiss(data =>{
        this.loadPlayList();
      });
      modal.present();
    }
    
  }
  