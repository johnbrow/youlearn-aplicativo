import { NavController } from 'ionic-angular';
import { UsuarioService } from './../../providers/usuario.service';
import { UtilService } from './../../providers/util.service';
import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-novo-usuario',
  templateUrl: 'novo-usuario.html',
})
export class NovoUsuarioPage {
  public form : FormGroup;
  constructor(private fb : FormBuilder, 
    private utilService: UtilService,
    private usuarioService: UsuarioService,
    private navController: NavController) 
    {
      this.form = this.fb.group(
        {
          primeiroNome: ['', Validators.compose([
            Validators.minLength(5),
            Validators.maxLength(50),
            Validators.required
          ])],
          ultimoNome: ['', Validators.compose([
            Validators.minLength(5),
            Validators.maxLength(50),
            Validators.required
          ])],
          email: ['', Validators.compose([
            Validators.minLength(5),
            Validators.maxLength(100),
            Validators.required
          ])],
          senha: ['', Validators.compose([
            Validators.minLength(5),
            Validators.maxLength(36),
            Validators.required
          ])],
          
        }
        );  
        
      }
      
      ionViewDidLoad() {
        
      }
      
      salvar(){
        let loading = this.utilService.showLoading();
        loading.present();
        console.log(this.form.value);
        this.usuarioService.Adicionar(this.form.value)
        .then((response)=>{
          
          //console.log(response.json());
          loading.dismiss();
          this.utilService.showAlert('Operação realizada com sucesso!');  
          localStorage.setItem('usuario.email', this.form.value.email);
          
          // REMOVE A PAGINA   
          this.navController.pop();
          
        })
        .catch((response)=>{
          loading.dismiss();
          
          this.utilService.showMessageError(response);          
          
        })
        
      }
      
      cancelar(){
        // REMOVE A PAGINA   
        this.navController.pop();
      }
    }
    