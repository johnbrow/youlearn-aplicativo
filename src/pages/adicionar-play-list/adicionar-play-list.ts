import { PlaylistService } from './../../providers/playlist.service';
import { UtilService } from './../../providers/util.service';
import { Component } from '@angular/core';
import { IonicPage, ViewController, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-adicionar-play-list',
  templateUrl: 'adicionar-play-list.html',
})
export class AdicionarPlayListPage {
  
  playlists: any[] = [];
  
  constructor(private ViewController: ViewController,
    private UtilService: UtilService,
    private playListService : PlaylistService,
    private AlertController : AlertController,
    ) {
    }
    
    ionViewDidLoad() {
      this.loadPlayList();
    }
    
    closeModal(){
      this.ViewController.dismiss();
    }
    
    excluir(playlist:any){
      //Abre a tela de aguarde
      let loading = this.UtilService.showLoading();
      loading.present();
      
      this.playListService.excluir(playlist.id).then((response)=>{
        
        this.UtilService.showAlert(response.json().message);
        
        // CARREGA LISTA DE CANAIS NOVAMENTE
        this.loadPlayList();
        
        // FECHA A TELA DE AGUARDE
        loading.dismiss();
        
      }).catch((response)=>{
        loading.dismiss();
        
        this.UtilService.showMessageError(response);
      });
    }
    
    loadPlayList(){
      // ABRE A TELA DE AGUARDE
      let loading = this.UtilService.showLoading();
      loading.present();
      
      this.playListService.listar()
      .then((response) => {
        this.playlists = response.json();
        
        //FECHA A TELA AGUARDE
        loading.dismiss();
      })
      .catch((response) =>{
        
        //FECHA A TELA AGUARDE
        loading.dismiss();
        
        this.UtilService.showMessageError(response);
      });
    }
    
    adicionar(){
      let prompt = this.AlertController.create({
        title: 'Adicionar playList',
        message: "Informe os Dados da playList",
        inputs: [
          {
            name: 'nome',
            placeholder: 'Nome da playList',
            type: 'text'
          }
        ],
        buttons: [
          {
            text : 'Cancelar',
            handler: data =>{
              
            }
          },
          {
            text : 'Salvar',
            handler: data =>{
              //Abre a tela de aguarde
              let loading = this.UtilService.showLoading();
              loading.present();

              this.playListService.adicionar(data.nome).then((response)=>{
                loading.dismiss();

                this.loadPlayList();
              }).catch((response)=>{
                loading.dismiss();
                
                this.UtilService.showMessageError(response);
              })
            }
          }
        ]
      });
      prompt.present();
    }
  }
  