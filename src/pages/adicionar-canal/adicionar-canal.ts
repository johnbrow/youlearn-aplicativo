import { CanalService } from './../../providers/canal.service';
import { UtilService } from './../../providers/util.service';
import { Component } from '@angular/core';
import { IonicPage, ViewController, AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-adicionar-canal',
  templateUrl: 'adicionar-canal.html',
})
export class AdicionarCanalPage {

  canais: any[] = [];
  constructor(private viewCtrl: ViewController,
    private UtilService: UtilService,
    private CanalService: CanalService,
    private AlertController: AlertController,
  ) {
  }

  ionViewDidLoad() {
    this.loadCanal();
  }

  loadCanal() {
    // ABRE A TELA DE AGUARDE
    let loading = this.UtilService.showLoading();
    loading.present();

    this.CanalService.listar()
      .then((response) => {
        this.canais = response.json();

        //FECHA A TELA AGUARDE
        loading.dismiss();
      })
      .catch((response) => {

        //FECHA A TELA AGUARDE
        loading.dismiss();

        this.UtilService.showMessageError(response);
      });
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  adicionar() {
    let prompt = this.AlertController.create({
      title: 'Adicionar Canal',
      message: "Informe os dados do Canal",
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome do Canal',
          type: 'text'
        },
        {
          name: 'urlLogo',
          type: 'text',
          placeholder: 'http://logo.jpg'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {

          }
        },
        {
          text: 'Salvar',
          handler: data => {
            // ABRE A TELA DE AGUARDE
            let loading = this.UtilService.showLoading();
            loading.present();

            this.CanalService.adicionar(data.nome, data.urlLogo).then((response)=>{
              // FECHA TELA DE AGUARDE
                loading.dismiss();

                this.loadCanal();

            }).catch((response)=>{
              loading.dismiss();
              this.UtilService.showMessageError(response);
           
            });
          }
        }
      ]
    });
    prompt.present();
  }

  excluir(canal:any){
    //Abre a tela de aguarde
    let loading = this.UtilService.showLoading();
    loading.present();

    this.CanalService.excluir(canal.id).then((response)=>{
      
      this.UtilService.showAlert(response.json().message);
      
      // CARREGA LISTA DE CANAIS NOVAMENTE
      this.loadCanal();

      // FECHA A TELA DE AGUARDE
      loading.dismiss();
    }).catch((response)=>{
      loading.dismiss();

      this.UtilService.showMessageError(response);
    });
  }
}
