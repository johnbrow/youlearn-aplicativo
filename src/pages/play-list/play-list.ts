import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { UtilService } from '../../providers/util.service';
import { VideoService } from '../../providers/video.service';

@IonicPage()
@Component({
  selector: 'page-play-list',
  templateUrl: 'play-list.html',
})
export class PlayListPage {
  
  idPlayList: any;
  nomePlayList: string;
  videos : any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private UtilService: UtilService,
    private VideoService: VideoService,) {
    this.idPlayList = this.navParams.get('idPlayList');
    this.idPlayList = this.navParams.get('nomePlayList');

  }

  ionViewDidLoad() {
    this.loadVideos(this.idPlayList);
  }

  compartilharFacebook(video){
    window.open('https://www.facebook.com/sharer.php?u='+video.url);

  } 
  loadVideos(idPlayList: any){
      // abrindo a tela de aguarde
      
      let loading = this.UtilService.showLoading();
      loading.present();
      
      this.VideoService.ListarPorPlayList(idPlayList).then(
        (response)=>{
          
          // populo a lista de videos com um array
          this.videos = response.json();
          
          // Fecho a tela de aguarde
          loading.dismiss();                         
        }
        ).catch((response)=>{
          this.UtilService.showMessageError(response);
        }
        
        );
  }

}
